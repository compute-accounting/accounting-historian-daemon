package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"

	"context"

	//Usage of the package which checking the created events
	"github.com/spf13/viper"
	"gitlab.cern.ch/compute-accounting/accounting-historian-daemon/formatter"
	"gitlab.cern.ch/compute-accounting/accounting-historian-daemon/parser"
)

// Runs every 30 minutes and searches for a new history file in the
// /accounting

func WatcherEvent(ctx context.Context, hf chan<- parser.HistoryFile, wg *sync.WaitGroup) {
	delay := (time.Duration(30)) * time.Minute
	defer wg.Done()

	for {
		select {
		case <-time.After(delay):
			SearchEvent(ctx, hf)
		case <-ctx.Done():
			return

		}
	}
}

// Searchs in the /accounting for the new history files that they
// have not the suffix .done because that means that they have
// already been parsed.
// Also, it chacks if the ModTime is less than one minute in
// order to exclude the new files for this time and parse them
// the next one

func SearchEvent(ctx context.Context, hf chan<- parser.HistoryFile) {
	err := filepath.Walk(viper.GetString(incomingPathKey), func(path string, info os.FileInfo, err error) error {
		if err != nil {
			fmt.Println("[ERROR]: ", err.Error())
			return err
		}
		if info.IsDir() {
			pattern := filepath.Join(path, "history.*")
			files, err := filepath.Glob(pattern)
			if err != nil {
				log.Println("[ERROR] Error searching for history files: ", err.Error())
				return nil
			}
			for _, file := range files {
				if strings.HasSuffix(file, "sync") {
					log.Println("[DEBUG] Ignoring synced file: ", file)
					continue
				}

				fileStatus, err := os.Stat(file)
				if err != nil {
					log.Println("[ERROR] ", err.Error())
					continue
				}
				if time.Since(fileStatus.ModTime()) < 1*time.Minute {
					continue
				}
				err = formatter.CheckFormat(file)
				if err != nil {

					log.Println("[DEBUG] No valid filename format", err.Error())
					continue
				}
				markedFile := fmt.Sprintf(".done.%s", filepath.Base(file))
				markDoneFile := filepath.Join(filepath.Dir(file), markedFile)

				_, err = os.Stat(markDoneFile)
				if err == nil {
					continue
				}
				historyFile := parser.HistoryFile{FileName: file, MarkedFileName: markDoneFile}
				select {
				case hf <- historyFile:
				case <-ctx.Done():
					return nil
				}
			}
		}
		return nil
	})
	if err != nil {
		log.Println("[ERROR]: ", err.Error())
	}

}

// Initializes teh yaml file with the constants which include
// the paths
func InitConfig(configFile string) {

	if configFile != "" {
		viper.SetConfigFile(configFile)
	} else {
		viper.AddConfigPath(".")
		viper.SetConfigName("config")
	}

	if err := viper.ReadInConfig(); err != nil {
		log.Println("[ERROR]: ", err.Error())
	}
}
