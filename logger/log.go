package logger

import (
	"log"
	"os"

	"github.com/hashicorp/logutils"
)

func NewLogger() {
	var loglevel string
	filter := &logutils.LevelFilter{
		Levels:   []logutils.LogLevel{"DEBUG", "INFO", "ERROR"},
		MinLevel: logutils.LogLevel(loglevel),
		Writer:   os.Stderr,
	}
	log.SetOutput(filter)
}
