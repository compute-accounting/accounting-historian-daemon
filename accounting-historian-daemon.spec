%global debug_package %{nil}

Summary:       Accounting historian daemon
Name:          accounting-historian-daemon
Version:       0.11
Release:       1%{?dist}
License:       Apache2 license
Group:         Development/Libraries
Source:        %{name}-%{version}.tar.gz
BuildRoot:     %{_tmppath}/%{name}-%{version}-root
Prefix:        %{_prefix}
BuildRequires: golang
ExclusiveArch: x86_64
Vendor:        batch-operations@cern.ch
Url:           https://gitlab.cern.ch/compute-accounting/accounting-historian-daemon

%{?systemd_requires}
BuildRequires: systemd

Requires: systemd

%description
Accounting historian daemon

%define debug_package %{nil}

%prep
%setup -q

%build
mkdir -p work/src/gitlab.cern.ch/compute-accounting/accounting-historian-daemon
export GOPATH=${PWD}/work
mv * work/src/gitlab.cern.ch/compute-accounting/accounting-historian-daemon/ || true
cd work/src/gitlab.cern.ch/compute-accounting/accounting-historian-daemon/
go build

%install
%{__rm} -rf %{buildroot}
mkdir -p ${RPM_BUILD_ROOT}/usr/bin
mkdir -p %{buildroot}/etc/accounting-historian
install -d %{buildroot}%{_unitdir}
install -p -m 644 work/src/gitlab.cern.ch/compute-accounting/accounting-historian-daemon/systemd/accounting-historian-daemon.service %{buildroot}/%{_unitdir}
install -p -m 644 work/src/gitlab.cern.ch/compute-accounting/accounting-historian-daemon/config.yaml ${RPM_BUILD_ROOT}/etc/accounting-historian/config.yaml
install -m 755 work/src/gitlab.cern.ch/compute-accounting/accounting-historian-daemon/accounting-historian-daemon ${RPM_BUILD_ROOT}/usr/bin

%clean
rm -rf ${RPM_BUILD_ROOT}

%post

# Preset on first install
%systemd_post accounting-historian-daemon.service

%preun

# Stops units before uninstalling
%systemd_preun accounting-historian-daemon.service

%postun

# Reload and restart units after installation
%systemd_postun_with_restart accounting-historian-daemon.service

%files
%defattr(-,root,root,-)
%{_bindir}/accounting-historian-daemon
%config(noreplace) /etc/accounting-historian/config.yaml
%{_unitdir}/accounting-historian-daemon.service


%changelog
* Fri Jun 07 2019 Nikolaos Petros Triantafyllidis <ntrianta@cern.ch> 0.11-1
- Added file ingore condition.
* Wed Jun 20 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.10-1
- Made signals handling better in order to kill subroutines softly.
* Tue Jun 12 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.9-1
- Fixed big lines issue in parse file.
* Mon Jun 11 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.8-1
- Added extra log infos for errors for mailer.
* Thu May 17 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.7-2
- Added struct for the under investigation history file.
- Added message for sending email in case of SIGTERM/SIGINT.
* Wed May 16 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.7-1
- Added CompletionDate check.
- Removed error bool chanel.
* Wed May 09 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.6-4
- Minor fixes.
* Wed May 09 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.6-3
- Added log path.
* Tue May 08 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.6-2
- Added limit for running routines simultaneously.
* Thu May 03 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.6-1
- Added a mailer as an alarm in case of a problem.
- Added data base updater for the dirty dates.
* Fri Apr 20 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.5-1
- Removed fsnotify watcher.
* Mon Apr 16 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.4-1
- Added context for handling signals.
* Thu Apr 12 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.3-1
- Added new logger.
* Thu Apr 12 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.2-5
- Added vendor for koji build.
* Thu Apr 12 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.2-3
- Installed the yaml file.
* Wed Apr 11 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.2-2
- Minor changes at file names.
* Tue Apr 10 2018 Florentia Protopsalti <florentia.protopsalti@cern.ch> 0.1-2
- Added accounting-historian-daemon service in systemd.
