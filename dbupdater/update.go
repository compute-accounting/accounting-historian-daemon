package dbupdater

import (
	"log"
	"os/exec"

	"github.com/spf13/viper"
)

func DbUpdate(dt string, hl string) error {
	args := []string{viper.GetString(configFilePath), viper.GetString(updateFunction), dt,
		viper.GetString(serviceName), viper.GetString(dbComment), "true", hl}
	_, err := exec.Command(viper.GetString(incomingPathKey), args...).Output()
	if err != nil {
		log.Println("[ERROR]: Data base update error", err.Error())
		return err
	}
	return nil
}
