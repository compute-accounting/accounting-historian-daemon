package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gitlab.cern.ch/compute-accounting/accounting-historian-daemon/logger"
	"gitlab.cern.ch/compute-accounting/accounting-historian-daemon/parser"
)

func main() {
	var wg sync.WaitGroup
	var wg2 sync.WaitGroup

	InitConfig("/etc/accounting-historian/config.yaml")
	logger.NewLogger()

	ctx, cancel := context.WithCancel(context.Background())
	// setup signal handler to catch SIGTERM/SIGINT
	signaled := make(chan os.Signal)
	signal.Notify(signaled, syscall.SIGINT, syscall.SIGTERM)

	defer func() {
		signal.Stop(signaled)
		cancel()
	}()

	hf := make(chan parser.HistoryFile)
	sem := make(chan bool, 3)

	f, err := os.OpenFile("/var/log/accounting-historian-daemon/accounting-historian-daemon.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Println("[ERROR]: ", err.Error())
	}

	defer f.Close()
	log.SetOutput(f)

	flag.Parse()

	wg.Add(3)
	go WatcherEvent(ctx, hf, &wg)
	go parser.Process(&wg, &wg2, ctx, sem, hf)

	go func() {
		defer wg.Done()
		select {
		case <-signaled:
			log.Println("[DEBUG]: SIGTERM/SIGINT")
			cancel()
		case <-ctx.Done():
		}
	}()
	wg.Wait()
}
