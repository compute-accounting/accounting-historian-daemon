# Accounting daemon for HTCondor history files

It scans the HTCondor per-day history files and searchs for dates in the classAds 
of the jobs which they do not belong to the history file's date. This is possible to happen
because many jobs quit the queue after ten days (e.g condor_submit -spool).

After finding the different dates, it creates hard links of the history file for these dates
in order to update the raw data in s3 for the new dates.
