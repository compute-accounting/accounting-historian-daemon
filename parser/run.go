package parser

import (
	"log"
)

// Creates a temporary path to store the history files

func Run(cp string) error {
	_, err := BuildTemporaryPath(cp)

	if err != nil {
		log.Println("[ERROR]: Build a temporary path issue", err.Error())
		return err
	}
	return nil
}
