package parser

import (
	"io"
	"log"
	"os"
)

func Copy(src, dst string) error {
	in, err := os.Open(src)
	if err != nil {
		log.Println("[ERROR]: ", err.Error())
	}
	defer in.Close()

	out, err := os.Create(dst)
	if err != nil {
		log.Println("[ERROR]: ", err.Error())
	}
	defer out.Close()

	_, err = io.Copy(out, in)
	if err != nil {
		log.Println("[ERROR]: ", err.Error())
	}
	return out.Close()
}
