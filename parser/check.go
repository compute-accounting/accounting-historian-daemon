package parser

import (
	"errors"
	"log"
)

func Check(temporaryDatesMap map[string]string) (string, error) {
	_, exist := temporaryDatesMap[CompletionDate]
	if exist {
		if (temporaryDatesMap[CompletionDate] == "0") || (temporaryDatesMap[CompletionDate] == "undefined") ||
			(temporaryDatesMap[CompletionDate] == "") {
			timeStamp, b := temporaryDatesMap[EnteredCurrentStatus]
			if (!b) || (timeStamp == "") || (timeStamp == "0") {
				error := errors.New("The EnteredCurrentStatus attribute does not exist or is equal to zero")
				log.Println("[ERROR]: ", error.Error())
				return "", error
			}
			return timeStamp, nil
		} else {
			timeStamp, b := temporaryDatesMap[CompletionDate]
			if !b {
				error := errors.New("The CompletionDate attribute does not exist")
				log.Println("[ERROR]: ", error.Error())
				return "", error
			}
			return timeStamp, nil
		}
	} else {
		timeStamp, b := temporaryDatesMap[EnteredCurrentStatus]
		if (!b) || (timeStamp == "") || (timeStamp == "0") {
			error := errors.New("The EnteredCurrentStatus attribute does not exist or is equal to zero")
			log.Println("[ERROR]: ", error.Error())
			return "", error
		}
		return timeStamp, nil
	}
}
