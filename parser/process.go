package parser

import (
	"log"
	"sync"

	"context"
)

func Process(wg *sync.WaitGroup, wg2 *sync.WaitGroup, ctx context.Context, sem chan bool, hf chan HistoryFile) {

	defer wg.Done()

	for {
		select {
		case <-ctx.Done():
			wg2.Wait()
			log.Println("[DEBUG]: The subroutines are completed")
			return
		case file := <-hf:
			sem <- true
			wg2.Add(1)
			go MarkFile(ctx, file, sem, wg2)

		}

	}
}
