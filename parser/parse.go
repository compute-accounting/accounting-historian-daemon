package parser

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"os"
	"strings"

	"gitlab.cern.ch/compute-accounting/accounting-historian-daemon/formatter"
)

const (
	EnteredCurrentStatus = "EnteredCurrentStatus = "
	CompletionDate       = "CompletionDate = "
	OffsetString         = "*** Offset ="
)

type Parser interface {
	Parse() (map[string]string, error)
	CreateHardLinkPath(map[string]string) error
}

type historyFileParser struct {
	pathToHistoryFile string
	newHistoryFile    string
	schedulerName     string
}

func HistoryFileParse(cf Parser) error {
	datesMap, err := cf.Parse()
	//Todo : set an alarm
	if err != nil {
		return err
	}
	err = cf.CreateHardLinkPath(datesMap)
	if err != nil {
		log.Println("[ERROR]: Problem to create hardlink", err.Error())
		return err
	}
	return nil
}

// Parse the history file in order to find the different dates
// and stores tem in a map

func (p historyFileParser) Parse() (map[string]string, error) {
	temporaryDatesMap := map[string]string{}
	datesMap := map[string]string{}

	if len(p.pathToHistoryFile) == 0 {
		err := errors.New("The path to the file does not exist")
		return nil, err
	}

	file, err := os.Open(p.pathToHistoryFile)

	if err != nil {
		log.Println("[ERROR]: ", err.Error())
	}

	defer file.Close()
	reader := bufio.NewReader(file)

	for {
		l, _, err := reader.ReadLine()
		line := string(l)
		if err == io.EOF {
			break
		}
		if err != nil {
			log.Println("[ERROR]: ", err.Error())
		}
		//Read the incoming file and extract the dates of the classAdd based on the "CompletionDate"
		// and if is equal to zero , based to "EnteredCurrentStatus".
		// Keeps both values in a map.

		complDate := strings.HasPrefix(line, CompletionDate)
		if complDate {
			splitLine := strings.SplitAfter(line, CompletionDate)
			temporaryDatesMap[splitLine[0]] = splitLine[1]
			continue
		}
		entCurrStatus := strings.HasPrefix(line, EnteredCurrentStatus)
		if entCurrStatus {
			splitLine := strings.SplitAfter(line, EnteredCurrentStatus)
			temporaryDatesMap[splitLine[0]] = splitLine[1]
			continue
		}
		// This line seperates the different classAds of the jobs
		offset := strings.Contains(line, OffsetString)
		if offset {
			if len(temporaryDatesMap) != 0 {
				timeStamp, err := Check(temporaryDatesMap)
				if err == nil {
					dates, err := formatter.MapDateFormat(timeStamp)
					if err != nil {
						log.Println("[ERROR]: Problem with Date Format", err.Error())
					}

					if _, ok := datesMap[dates]; !ok { // Check if the date already exists, if not put in the map
						datesMap[dates] = dates
						log.Println("[DEBUG] The dates are: ", dates)
					}
				} else {
					log.Println(err.Error())
				}
				temporaryDatesMap = make(map[string]string)
			}
		}
	}
	if len(datesMap) == 0 {
		mapError := fmt.Sprintf("Dates Map is empty %s ", p.pathToHistoryFile)
		err := errors.New(mapError)
		return nil, err
	}
	return datesMap, nil
}
