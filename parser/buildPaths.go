package parser

import (
	"log"
	"os"

	"gitlab.cern.ch/compute-accounting/accounting-historian-daemon/formatter"
)

func BuildTemporaryPath(cp string) (string, error) {
	pathInfo, err := formatter.PathFormat(cp)
	if err != nil {
		log.Println("[ERROR]: Path Format error", err.Error())
		return "", err
	}
	CreatePath(pathInfo.FilePath + pathInfo.FormFileDate)
	newTempPath := (pathInfo.FilePath + pathInfo.FormFileDate + pathInfo.FileName)

	err = Copy(cp, newTempPath)

	if err != nil {
		log.Println("[ERROR]: Unable to copy history file to temporary destination", err)
		return "", err
	}
	//Copy the file from the temporary path to the new one based on the name of the schedd,the date, etc
	arguments := historyFileParser{pathToHistoryFile: newTempPath, newHistoryFile: pathInfo.FileName, schedulerName: pathInfo.SchedulerName}
	err = HistoryFileParse(arguments)
	if err != nil {
		log.Println("[ERROR]: Cannot parse the history file", err)
		return "", err
	}
	return pathInfo.FileName, nil
}

func CreatePath(dynamicPath string) {
	if _, err := os.Stat(dynamicPath); os.IsNotExist(err) {
		os.MkdirAll(dynamicPath, 0777)
	}

}
