package parser

import (
	"log"
	"os"

	"github.com/spf13/viper"
	"gitlab.cern.ch/compute-accounting/accounting-historian-daemon/dbupdater"
)

// Based on how many different days exist in the history file,
// creates hard links of the the original history file. The new
// files will be stored based on the different date

func (f historyFileParser) CreateHardLinkPath(datesMap map[string]string) error {

	for k := range datesMap { // The value of the key in the map is the date
		dynamicPath := viper.GetString(hardLinkPathKey) + f.schedulerName + "/" + k + "/"
		CreatePath(dynamicPath)
		hardLink := dynamicPath + f.newHistoryFile

		log.Println("[INFO] The hardlink path is: ", hardLink)

		if _, err := os.Stat(hardLink); os.IsNotExist(err) {
			err = os.Link(f.pathToHistoryFile, hardLink)

			if err != nil {
				log.Println("[ERROR] : Cannot create hardlink", err.Error())
				return err
			}
			err := dbupdater.DbUpdate(k, hardLink)
			if err != nil {
				log.Println("[ERROR] : ", err.Error())
				return err
			}
		}
	}
	return nil
}
