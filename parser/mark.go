package parser

import (
	"log"
	"os"
	"sync"

	"context"

	"gitlab.cern.ch/compute-accounting/accounting-historian-daemon/mailer"
)

type HistoryFile struct {
	FileName       string
	MarkedFileName string
}

func MarkFile(ctx context.Context, hf HistoryFile, sem chan bool, wg2 *sync.WaitGroup) {
	defer wg2.Done()
	select {
	case <-ctx.Done():
		log.Println("[DEBUG] Waiting Goroutines to be completed")
		return

	default:
		file := hf.FileName
		markedFile := hf.MarkedFileName
		err := Run(file)
		if err != nil {
			mailer.SendMail(err.Error())
			log.Println("[ERROR] Parsing issues: ", err.Error())
			return
		} else {
			_, err = os.Create(markedFile)
			if err != nil {
				log.Println("[ERROR] Cannot create mark file: ", err.Error())
				mailer.SendMail(err.Error())
				return
			}
		}

		<-sem

	}

}
