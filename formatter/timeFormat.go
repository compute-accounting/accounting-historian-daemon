package formatter

import (
	"fmt"
	"log"
	"strconv"
	"strings"
	"time"
)

// Fix the date format in order to put them in the map
func MapDateFormat(cp string) (string, error) {
	trimmedValue := strings.TrimSpace(cp)
	unixIntValue, err := strconv.ParseInt(trimmedValue, 10, 64)

	if err != nil {
		log.Println("[ERROR]: Something went wrong with the unix value of time", err.Error())
		return "", err
	}
	timeStamp := time.Unix(unixIntValue, 0)
	dates := fmt.Sprintf("%s", timeStamp.Format("2006/01/02"))
	return dates, nil
}
