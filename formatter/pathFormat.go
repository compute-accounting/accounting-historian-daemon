package formatter

import (
	"errors"
	"strconv"
	"strings"

	"github.com/spf13/viper"
)

type Path struct {
	FormFileDate  string
	FilePath      string
	FileName      string
	SchedulerName string
}

func PathFormat(fp string) (*Path, error) {
	newHistoryFile := strings.Split(fp, "/")
	filePath := viper.GetString(tempTargetDir) + newHistoryFile[len(newHistoryFile)-2]
	fileNameSplit := strings.Split(newHistoryFile[len(newHistoryFile)-1], "history.")
	fileName := fileNameSplit[len(fileNameSplit)-1]
	fileDate := fileName[0:6]
	formFileDate := "/" + fileDate[0:4] + "/" + fileDate[4:6] + "/"
	path := Path{FormFileDate: formFileDate, FilePath: filePath, FileName: newHistoryFile[len(newHistoryFile)-1],
		SchedulerName: newHistoryFile[len(newHistoryFile)-2]}
	return &path, nil
}

func IsNumeric(s string) bool {
	_, err := strconv.ParseFloat(s, 64)
	return err == nil
}

func CheckFormat(fp string) error {
	newHistoryFile := strings.Split(fp, "/")
	fileNameSplit := strings.Split(newHistoryFile[len(newHistoryFile)-1], "history.")
	fileName := fileNameSplit[len(fileNameSplit)-1]
	if (len(fileName) < 10) || !(IsNumeric(fileName[0:6])) {
		err := errors.New("Invalid filename format")
		return err
	}
	return nil
}
