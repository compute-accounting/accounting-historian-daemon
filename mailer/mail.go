package mailer

import (
	"crypto/tls"
	"log"

	gomail "gopkg.in/gomail.v2"
)

func SendMail(msg string) {

	MailConf(msg)
	log.Println("[ERROR]: Process error", msg)

}
func MailConf(mes string) {

	m := gomail.NewMessage()
	m.SetHeader("From", "florentia.protopsalti@cern.ch")
	m.SetHeader("To", "florentia.protopsalti@cern.ch")
	m.SetHeader("Subject", "Historian Daemon errors")
	m.SetBody("text/html", mes)

	d := gomail.NewDialer("localhost", 25, "", "")

	d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	d.DialAndSend(m)
}
